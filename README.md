
# **Standard QA Documents**

  
## **Test Plan and a Test Progress Report**

![Test plan and progress report](https://images.ctfassets.net/6xhdtf1foerq/74Um8nSYFO8ySa6cM1N6dk/34d12c07d30a546fa3ac9cc31cd5f9ea/Screenshot_2020-02-26_at_17.11.17.png)
  

  

## **Test Cases**
Test cases are pretty simple – this QA documentation consists of 7 sections: ID, Test Case, Testing Steps, Expected Result, Status, Actual Result, and Comments.

1.  **ID**  is a unique number assigned to your test case.
2.  In the  **Test Case**  section, you point out the requirement(s) you will be testing and provide a link to it in the specifications document.
3.  In the  **Testing Steps**  section, you list all the actions needed to complete a test case.
4.  In the  **Expected Result** section, you summarize the outcome of a particular test if it is successful.
5.  In the  **Status**  section, you indicate if a particular step passed or failed testing.
6.  In the  **Actual Result**  section, you explain the outcome of a failed test.
7.  The  **Comments**  section is not obligatory, but you can add it to leave some additional notes.

![enter image description here](https://images.ctfassets.net/6xhdtf1foerq/2z2qA5baAwNJ9hz0gdy1gW/949884b62768fbc35efc4ea7d1c21962/image1.png)
  

  
  
## **Defect Report**

The defect report consists of the following sections: identifier, summary, description, steps to reproduce, reproducibility, severity, priority, environment, and attachments.

1.  Each particular software issue is assigned a unique number – the **identifier**. It optimizes navigation through QA documents and facilitates communication between developers, testers, and PMs.
2.  In the **summary** section, you provide a concise answer to three simple questions: what happened, where, and under what circumstances.
3.  In the **description** section, you describe the bug in detail. You should tell about the actual results and the expected ones. It's useful to provide a link to your software requirements.
4.  Then, you write about the **steps to reproduce (STR)**. This shows developers exactly how to reproduce the issue. Don't miss a step or your report may return to you.
5.  In the **reproducibility** section, you clarify if the bug appears every time you follow the STR. You should use numbers to show approximate chances, for example 7 times out of 10.
6.  In the **severity** section, you explain how much harm the bug may bring to the project. In other words, it shows the technological degree of influence of the defect on the entire system. Even a small issue may badly affect the entire application.
7.  **Priority** shows how important a particular defect report is. Priority can be denoted with the help of letters – “A” for the most urgent and “Z” for the least urgent, numbers – “1” for the most urgent and “9” for the least urgent, or simply words like “high”, “medium”, or “low”.
8.  In the **environment** section, you should define which operating systems or browser versions were affected.
9.  Finally, the **attachments** include the list of videos, screenshots, or console logs files added to the defect report.

 
![enter image description here](https://images.ctfassets.net/6xhdtf1foerq/2n913S9f8xvlCpmy83uJlK/c9f77cff11761ef9b9cbfbac7af67531/image5.png) 

The first step is to compile and  **submit**  the defect report. At this point, the Project Manager or a tech lead decides whether to  **assign**  it to a developer or to  **decline**  it on the grounds of insufficiency or inadequacy.

After the assigned developer has  **fixed**  the bug, you as a QA have to double-check and  **verify**  it has been fixed. If yes, you can  **close**  the defect report. The best practice is to close it in a week or two.

If the bug is not fixed, you  **reopen**  the defect report and send it back to the assigned developer. The bug-fixing process can be a long one, but you have to stay strong until all the defect reports are finally closed.
